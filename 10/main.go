package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"utilities"
)

func main() {
	input := GetInput("./input.live.txt")
	fmt.Println(PartA(input))
	fmt.Println(PartB(input))
}

func PartA(input input) int {
	cycle := 1
	x := 1
	signalStrength := 0

	for _, a := range getCycleData(input.data) {
		cycle++
		x += a

		if (cycle - 20)%40 == 0 {
			signalStrength += (cycle * x)
		}
	}

	return signalStrength
}

func PartB(input input) string {
	cycle := 0
	x := 1
	output := ""

	for _, a := range getCycleData(input.data) {
		pos := cycle%40
		s := "."
		if pos == 0 {
			output += "\n"
		}
		if pos == x-1 || pos == x || pos == x+1 {
			s = "#"
		}

		output += s
		cycle++
		x += a
	}

	return output
}

func GetInput(filepath string) input {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	stringInput := utilities.ParseFileHandler(file)

	return input{data: stringInput}
}

func parseLine(line string) (cycles int, add int) {
	split := strings.Split(line, " ")
	if len(split) == 1 {
		return 1, 0
	}
	if len(split) == 2 {
		add, err := strconv.Atoi(split[1])
		if err != nil {
			panic(err)
		}
		return 2, add
	}

	panic("Command could not be parsed correctly. Command = '" + line + "'")
}

func getCycleData(data []string) []int {
	var cycles []int
	for _, l := range data {
		c, a := parseLine(l)
		for i := 0; i < c; i++ {
			if i == 0 {
				cycles = append(cycles, 0)
			} else if i == 1 {
				cycles = append(cycles, a)
			} else {
				panic("too many cycles")
			}
		}
	}
	return cycles
}

type input struct {
	data []string
}
