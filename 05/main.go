package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"utilities"
)

func main() {
	input := GetInput("./input.live.txt")
	// fmt.Println(PartA(input))
	fmt.Println(PartB(input))
}

func PartA(input input) string {
	for _, move := range input.moves {
		split := strings.Split(move, " ")
		boxes, err := strconv.Atoi(split[1])
		if err != nil {
			panic(err)
		}
		source, err := strconv.Atoi(split[3])
		if err != nil {
			panic(err)
		}
		source -= 1
		dest, err := strconv.Atoi(split[5])
		if err != nil {
			panic(err)
		}
		dest -= 1

		for i := 0; i < boxes; i++ {
			box := input.stacks[source][len(input.stacks[source])-1]
			input.stacks[source] = input.stacks[source][:len(input.stacks[source])-1]
			input.stacks[dest] = append(input.stacks[dest], box)
		}
	}

	var chars string
	for _, stack := range input.stacks {
		chars += string(stack[len(stack)-1])
	}
	return chars
}

func PartB(input input) string {
	for _, move := range input.moves {
		split := strings.Split(move, " ")
		boxes, err := strconv.Atoi(split[1])
		if err != nil {
			panic(err)
		}
		source, err := strconv.Atoi(split[3])
		if err != nil {
			panic(err)
		}
		source -= 1
		dest, err := strconv.Atoi(split[5])
		if err != nil {
			panic(err)
		}
		dest -= 1

		for _, b := range input.stacks[source][len(input.stacks[source])-boxes:] {
			input.stacks[dest] = append(input.stacks[dest], b)
		}
		input.stacks[source] = input.stacks[source][:len(input.stacks[source])-boxes]

	}

	var chars string
	for _, stack := range input.stacks {
		chars += string(stack[len(stack)-1])
	}
	return chars
}

func GetInput(filepath string) input {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	stringInput := utilities.ParseFileHandler(file)
	rowIdLineIdx := 0
	for i, l := range stringInput {
		if l == "" {
			rowIdLineIdx = i - 1
			break;
		}
	}
	rowIdString := strings.ReplaceAll(stringInput[rowIdLineIdx], " ", "")
	var rowIdArray []int
	for _, s := range rowIdString {
		i, err := strconv.Atoi(string(s))
		if err != nil {
			panic(err)
		}
		rowIdArray = append(rowIdArray, i)
	}

	var stacks [][]rune
	for i := range rowIdArray {
		var stack []rune
		col := 1 + (4 * i)
		for j := rowIdLineIdx -1; j >= 0; j-- {
			if stringInput[j][col] == ' ' {
				break
			}
			stack = append(stack, rune(stringInput[j][col]))
		}
		stacks = append(stacks,  stack)
	}

	return input{stacks: stacks, moves: stringInput[rowIdLineIdx+2:]}
}

type input struct {
	stacks [][]rune
	moves []string
}
