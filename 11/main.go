package main

import (
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
	"utilities"
)

func main() {
	input := GetInput("./input.live.txt")
	fmt.Println(PartA(input))
	fmt.Println(PartB(input))
}

func PartA(monkeys []monkey) int {
	return solve(monkeys, 20, true)
}

func PartB(monkeys []monkey) int {
	return solve(monkeys, 10000, false)
}

func solve(monkeys []monkey, rounds int, divideLevels bool) int {
	commonMultiple := 1
	for _, m := range monkeys {
		commonMultiple *= m.divisor
	}

	for i := 0; i < rounds; i++ {
		for idx := range monkeys {
			for len(monkeys[idx].items) > 0 {
				item := monkeys[idx].items[0]
				if item > commonMultiple {
					panic(item)
				}
				item = monkeys[idx].operation(item) % commonMultiple
				if item > commonMultiple {
					panic(item)
				}
				if (divideLevels) {
					item = int(math.Floor(float64(item) / 3))
				}
				nextMonkey := monkeys[idx].nextPass(item)
				monkeys[nextMonkey].items = append(monkeys[nextMonkey].items, item)
				monkeys[idx].items = monkeys[idx].items[1:]
				monkeys[idx].inspectedItems++
			}
		}
	}
	
	var inspections []int
	for _, m := range monkeys {
		inspections = append(inspections, m.inspectedItems)
	}
	sort.Ints(inspections)
	fmt.Println(inspections)

	return inspections[len(inspections) - 1 ] * inspections[len(inspections) - 2]
}

func GetInput(filepath string) []monkey {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	stringInput := utilities.ParseFileHandler(file)
	monkeyInputs := batchLines(stringInput, 7)

	var monkeys []monkey
	for _, i := range monkeyInputs {
		monkeys = append(monkeys, parseMonkey(i))
	}
	
	return monkeys
}


func batchLines(lines []string, size int) [][]string {
	var batches [][]string
    for i := 0; i < len(lines); i += size {
        batch := lines[i:min(i+size, len(lines))]
        batches = append(batches, batch)
    }
	return batches
}

func min(a, b int) int {
    if a <= b {
        return a
    }
    return b
}

func parseMonkey(input []string) monkey {
	return monkey{
		items: parseItems(input[1]),
		operation: parseOperation(input[2]),
		nextPass: parseNextPass(input[3:6]),
		divisor: parseDivisor(input[3]),
	}
}

func parseItems(line string) []int {
	var items []int

	itemsString := strings.Replace(line, "  Starting items: ", "", 1)
	itemsStrings := strings.Split(itemsString, ", ")

	for _, s := range itemsStrings {
		i, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		items = append(items, i)
	}

	return items
}

func parseOperation(line string) func(int) int {
	operationString := strings.Replace(line, "  Operation: new = old ", "", 1)
	operationSplit := strings.Split(operationString, " ")

	var operationValue int
	var err error
	if operationSplit[1] != "old" {
		operationValue, err = strconv.Atoi(operationSplit[1])
		if err != nil {
			panic(err)
		}
	}

	operator := operationSplit[0]
	if operator == "*" {
		return func(i int) int {
			if operationValue == 0 {
				return i * i
			}
			return i * operationValue
		}
	}

	if operator == "+" {
		return func(i int) int {
			if operationValue == 0 {
				return i + i
			}
			return i + operationValue
		}
	}

	panic("Unexpected operator '" + operator + "'")
}

func parseDivisor(line string) int {
	divValStr := strings.Replace(line, "  Test: divisible by ", "", 1)
	divVal, err := strconv.Atoi(divValStr)
	if err != nil {
		panic(err)
	}

	return divVal
}

func parseNextPass(lines []string) func(int) int {
	divVal := parseDivisor(lines[0])
	trueValStr := strings.Replace(lines[1], "    If true: throw to monkey ", "", 1)
	trueVal, err := strconv.Atoi(trueValStr)
	if err != nil {
		panic(err)
	}
	
	falseValStr := strings.Replace(lines[2], "    If false: throw to monkey ", "", 1)
	falseVal, err := strconv.Atoi(falseValStr)
	if err != nil {
		panic(err)
	}

	return func(i int) int {
		if i % divVal == 0 {
			return trueVal
		}
		return falseVal
	}
}

type monkey struct {
	items     []int
	operation func(int) int
	nextPass  func(int) int
	inspectedItems int
	divisor int
}
