package main

import (
	"fmt"
	"math"
	"os"
	"utilities"
)

func main() {
	input := GetInput("./input.live.txt")
	fmt.Println(PartA(input))
	fmt.Println(PartB(input))
}

var chars string = "abcdefghijklmnopqrstuvwxyz"

func PartA(input input) int {
	return findMarker(4, input.data)
}

func PartB(input input) int {
	return findMarker(14, input.data)
}

func findMarker(markerLength int, signal string) int {
	var buffer []int = make([]int, markerLength)
	for idx := range signal {
		if len(signal) < idx+4 {
			break
		}

		for bidx := range buffer {
			buffer[bidx] = idxToBinInt(charToIdx(signal[idx+bidx]))
		}

		test := 0
		for _, b := range buffer {
			test |= b
		}
		bstring := fmt.Sprintf("%b", test)

		c := 0
		for _, b := range bstring {
			if b == '1' {
				c++
			}
		}

		if c == len(buffer) {
			return idx + len(buffer)
		}
	}

	panic("no marker found!")
}

func charToIdx(b byte) int {
	for idx := range chars {
		if b == chars[idx] {
			return idx
		}
	}
	panic("could not finc byte in char string: " + string(b))
}

func idxToBinInt(idx int) int {
	return int(math.Pow(2.0, float64(idx)))
}

func GetInput(filepath string) input {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	stringInput := utilities.ParseFileHandler(file)

	return input{data: stringInput[0]}
}

type input struct {
	data string
}
