package utilities

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func ParseFileHandler(_file *os.File) []string {
	scanner := bufio.NewScanner(_file)

	var output []string
	for scanner.Scan() {
		output = append(output, scanner.Text())
	}
	return output
}
	

func ParseByFileName(_filepath string) []string {
	inputBytes, err := os.ReadFile(_filepath)
	if err != nil {
		panic(err)
	}

	output := strings.Split(string(inputBytes), "\n")
	return output
}

func ConvertInputToInt(input []string) []int {
	var intInput []int
	for _, line := range input {
		intLine, err := strconv.ParseInt(line, 10, 0)
		if err != nil {
			panic(err)
		}
		intInput = append(intInput, int(intLine))
	}

	return intInput
}