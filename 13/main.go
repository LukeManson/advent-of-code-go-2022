package main

import (
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"utilities"
)

var arrayType reflect.Type = reflect.TypeOf([]any{})
var floatType reflect.Type = reflect.TypeOf(0.1)

func main() {
	input := GetInput("./input.live.txt")
	fmt.Println(PartA(input))
	fmt.Println(PartB(input))
}

func unmarshalSignal(a string) []any {
	var arr []any

	err := json.Unmarshal([]byte(a), &arr)
	if err != nil {
		fmt.Println(a)
		panic(err)
	}

	return arr
}


func compareSignals(aMap []any, bMap []any) (corrrect bool, undecided bool) {
	aLen := len(aMap)
	bLen := len(bMap)
	minSize := aLen
	if bLen < minSize {
		minSize = bLen
	}

	for i := 0; i < minSize; i++ {
		a := aMap[i]
		b := bMap[i]
		aType := reflect.TypeOf(a)
		bType := reflect.TypeOf(b)
		if aType == floatType && bType == floatType {
			af := a.(float64)
			bf := b.(float64)
			if af < bf {
				return true, false
			} else if bf < af {
				return false, false
			}
		} else if aType == arrayType && bType == arrayType {
			c, u := compareSignals(a.([]any), b.([]any))
			if !u {
				return c, u
			}
		} else if aType == floatType && bType == arrayType {
			c, u := compareSignals([]any{a.(float64)}, b.([]any))
			if !u {
				return c, u
			}
		} else if aType == arrayType && bType == floatType {
			c, u := compareSignals(a.([]any), []any{b.(float64)})
			if !u {
				return c, u
			}
		}
	}

	if aLen != bLen{
		if aLen == minSize {
			return true, false
		}
		if bLen == minSize {
			return false, false
		}
	}

	return false, true
}

func PartA(input input) int {
	correctSignalSum := 0
	for idx, p := range input.pairs {
		p0 := unmarshalSignal(p[0])
		p1 := unmarshalSignal(p[1])
		correct, undecided := compareSignals(p0, p1)
		if !undecided && correct {
			correctSignalSum += idx + 1
		}
	}

	return correctSignalSum
}

func PartB(input input) int {
	signalPairs := input.pairs
	var signals [][]any
	for _, p := range signalPairs {
		signals = append(signals, unmarshalSignal(p[0]))
		signals = append(signals, unmarshalSignal(p[1]))
	}

	var orderedSignals [][]any
	for _, s := range signals {
		if len(orderedSignals) == 0 {
			orderedSignals = append(orderedSignals, s)
			continue
		}

		found := false
		for idx, os := range orderedSignals {
			correct, undecided := compareSignals(s, os)
			if !undecided && correct {
				orderedSignals = insert(orderedSignals, idx, s)
				found = true
				break
			}
		}

		if !found {
			orderedSignals = insert(orderedSignals, len(orderedSignals), s)
		}
	}

	decoderKey := 1
	extraSignals := [][]any{unmarshalSignal("[[2]]"), unmarshalSignal("[[6]]")}
	for _, e := range extraSignals {
		found := false
		for idx, os := range orderedSignals {
			correct, undecided := compareSignals(e, os)
			if !undecided && correct {
				orderedSignals = insert(orderedSignals, idx, e)
				found = true
				decoderKey *= idx + 1
				break
			}
		}

		if !found {
			orderedSignals = insert(orderedSignals, len(orderedSignals), e)
			decoderKey *= len(orderedSignals) + 1
		}
	}

	return decoderKey
}


func insert(a [][]any, index int, value []any) [][]any {
    if len(a) == index {
        return append(a, value)
    }
    a = append(a[:index+1], a[index:]...)
    a[index] = value
    return a
}

func GetInput(filepath string) input {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	stringInput := utilities.ParseFileHandler(file)
	idx := 0
	var pair [2]string
	var pairs [][2]string
	for _, l := range stringInput {
		if l == "" {
			pairs = append(pairs, pair)
			idx = 0
			continue
		}
		pair[idx] = l
		idx++
	}

	return input{pairs: pairs}
}

type input struct {
	pairs [][2]string
}
