package main

import (
	"fmt"
	"os"
	"utilities"
)

func main() {
	input := GetInput("./input.live.txt")
	fmt.Println(PartA(input))
	fmt.Println(PartB(input))
}

func PartA(input input) int {
	return 0
}

func PartB(input input) int {
	return 0
}

func GetInput(filepath string) input {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	stringInput := utilities.ParseFileHandler(file)

	return input{data: stringInput}
}

type input struct {
	data []string
}
