package main

import (
	"fmt"
	"testing"
)

var inputFileName = "./input.test.txt"
var expectedA = 0;
var expectedB = 0;

func TestPartA(t *testing.T) {
	input := GetInput(inputFileName)
	expected := expectedA
	if expectedA == 0 {
		t.Skip("Skip A")
	}
	actual := PartA(input)

	if actual != expected {
		t.Log("Expected: " + fmt.Sprint(expected))
		t.Log("Actual: " + fmt.Sprint(actual))
		t.Fail()
	}
}

func TestPartB(t *testing.T) {
	input := GetInput(inputFileName)
	expected := expectedB
	if expectedB == 0 {
		t.Skip("Skip B")
	}
	actual := PartB(input)

	if actual != expected {
		t.Log("Expected: " + fmt.Sprint(expected))
		t.Log("Actual: " + fmt.Sprint(actual))
		t.Fail()
	}
}
