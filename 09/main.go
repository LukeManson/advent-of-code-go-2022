package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"utilities"
)

func main() {
	input := GetInput("./input.live.txt")
	fmt.Println(PartA(input))
	fmt.Println(PartB(input))
}

func PartA(input input) int {
	return solve(input.data, 2)
}

func PartB(input input) int {
	return solve(input.data, 10)
}

func GetInput(filepath string) input {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	stringInput := utilities.ParseFileHandler(file)

	return input{data: stringInput}
}

func solve(input []string, ropeLength int) int {
	moves := parseMoves(input)
	var positions [][2]int
	for i := 0; i < ropeLength; i++ {
		positions = append(positions, [2]int{0, 0})
	}
	var positionsVisited = make(map[[2]int]int)
	positionsVisited[positions[len(positions) - 1]] = 1

	for _, m := range moves {
		positions = moveRopeSection(positions, m)
		positionsVisited[positions[len(positions) - 1]]++
	}

	return len(positionsVisited)
}

func moveRopeSection(positions [][2]int, move move) [][2]int {
	positions[0][0] += move.x
	positions[0][1] += move.y

	for p := 1; p < len(positions); p++ {
		if positions[p-1][0] > positions[p][0]+1 {
			if positions[p-1][1] > positions[p][1] {
				positions[p][1]++
			} else if positions[p-1][1] < positions[p][1] {
				positions[p][1]--
			}
			positions[p][0]++
		} else if positions[p-1][0] < positions[p][0]-1 {
			if positions[p-1][1] > positions[p][1] {
				positions[p][1]++
			} else if positions[p-1][1] < positions[p][1] {
				positions[p][1]--
			}
			positions[p][0]--
		} else if positions[p-1][1] > positions[p][1]+1 {
			if positions[p-1][0] > positions[p][0] {
				positions[p][0]++
			} else if positions[p-1][0] < positions[p][0] {
				positions[p][0]--
			}
			positions[p][1]++
		} else if positions[p-1][1] < positions[p][1]-1 {
			if positions[p-1][0] > positions[p][0] {
				positions[p][0]++
			} else if positions[p-1][0] < positions[p][0] {
				positions[p][0]--
			}
			positions[p][1]--
		}
	}

	return positions
}

func parseMoves(input []string) []move {
	var moves []move
	for _, l := range input {
		x := 0
		y := 0

		split := strings.Split(l, " ")
		if len(split) != 2 {
			panic("Could not parse input. Unexpected format. Input = '" + l + "'")
		}

		dist, err := strconv.Atoi(split[1])
		if err != nil {
			panic(err)
		}

		if split[0] == "U" {
			x = 0
			y = -1
		} else if split[0] == "D" {
			x = 0
			y = 1
		} else if split[0] == "R" {
			x = 1
			y = 0
		} else if split[0] == "L" {
			x = -1
			y = 0
		} else {
			panic("Could not parse direction. Input = '" + l + "'")
		}

		for i := 0; i < dist; i++ {
			moves = append(moves, move{x, y})
		}
	}

	return moves
}

type move struct {
	x int
	y int
}

type input struct {
	data []string
}
