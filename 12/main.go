package main

import (
	"fmt"
	"os"
	"utilities"
)

func main() {
	input := GetInput("./input.live.txt")
	// fmt.Println(PartA(input))
	fmt.Println(PartB(input))
}

func getMinDist(vWithD []vertex, sptSet map[[2]int]bool) vertex {
	var minDist vertex
	set := false
	for _, v := range vWithD {
		if !sptSet[v.position] && (!set || v.distance < minDist.distance) {
			set = true
			minDist = v
		}
	}

	return minDist
}

func PartA(input input) int {
	vertices := input.data
	sptSet := input.sptSet
	var verticesWithDistance []vertex

	vertices[input.start[0]][input.start[1]].inf = false
	verticesWithDistance = append(verticesWithDistance, vertices[input.start[0]][input.start[1]])

	for i := 0; i < 50000; i++ {
		minDist := getMinDist(verticesWithDistance, sptSet)
		sptSet[minDist.position] = true
		vertices[minDist.position[0]][minDist.position[1]].visited = true
	
		y := minDist.position[0] - 1
		x := minDist.position[1]
		if y >= 0 && !sptSet[[2]int{y,x}] && vertices[y][x].height < minDist.height + 2 {
			vertices[y][x].inf = false
			vertices[y][x].distance = minDist.distance + 1
			verticesWithDistance = append(verticesWithDistance, vertices[y][x])
			if vertices[y][x].end {
				return vertices[y][x].distance
			}
		}
	
		y = minDist.position[0] + 1
		x = minDist.position[1]
		if y < len(vertices) && !sptSet[[2]int{y,x}] && vertices[y][x].height < minDist.height + 2 {
			vertices[y][x].inf = false
			vertices[y][x].distance = minDist.distance + 1
			verticesWithDistance = append(verticesWithDistance, vertices[y][x])
			if vertices[y][x].end {
				return vertices[y][x].distance
			}
		}
	
		y = minDist.position[0]
		x = minDist.position[1] - 1
		if x >= 0 && !sptSet[[2]int{y,x}] && vertices[y][x].height < minDist.height + 2 {
			vertices[y][x].inf = false
			vertices[y][x].distance = minDist.distance + 1
			verticesWithDistance = append(verticesWithDistance, vertices[y][x])
			if vertices[y][x].end {
				return vertices[y][x].distance
			}
		}
	
		y = minDist.position[0]
		x = minDist.position[1] + 1
		if x < len(vertices[0]) && !sptSet[[2]int{y,x}] && vertices[y][x].height < minDist.height + 2 {
			vertices[y][x].inf = false
			vertices[y][x].distance = minDist.distance + 1
			verticesWithDistance = append(verticesWithDistance, vertices[y][x])
			if vertices[y][x].end {
				return vertices[y][x].distance
			}
		}
	}


	return 0
}

func PartB(input input) int {
	vertices := input.data
	sptSet := input.sptSet
	var verticesWithDistance []vertex

	vertices[input.end[0]][input.end[1]].inf = false
	verticesWithDistance = append(verticesWithDistance, vertices[input.end[0]][input.end[1]])

	for i := 0; i < 500000; i++ {
		minDist := getMinDist(verticesWithDistance, sptSet)
		sptSet[minDist.position] = true
		vertices[minDist.position[0]][minDist.position[1]].visited = true
	
		y := minDist.position[0] - 1
		x := minDist.position[1]
		if y >= 0 && !sptSet[[2]int{y,x}] && vertices[y][x].height > minDist.height - 2 {
			vertices[y][x].inf = false
			vertices[y][x].distance = minDist.distance + 1
			verticesWithDistance = append(verticesWithDistance, vertices[y][x])
			if vertices[y][x].height == 0 {
				return vertices[y][x].distance
			}
		}
	
		y = minDist.position[0] + 1
		x = minDist.position[1]
		if y < len(vertices) && !sptSet[[2]int{y,x}] && vertices[y][x].height > minDist.height - 2 {
			vertices[y][x].inf = false
			vertices[y][x].distance = minDist.distance + 1
			verticesWithDistance = append(verticesWithDistance, vertices[y][x])
			if vertices[y][x].height == 0 {
				return vertices[y][x].distance
			}
		}
	
		y = minDist.position[0]
		x = minDist.position[1] - 1
		if x >= 0 && !sptSet[[2]int{y,x}] && vertices[y][x].height > minDist.height - 2 {
			vertices[y][x].inf = false
			vertices[y][x].distance = minDist.distance + 1
			verticesWithDistance = append(verticesWithDistance, vertices[y][x])
			if vertices[y][x].height == 0 {
				return vertices[y][x].distance
			}
		}
	
		y = minDist.position[0]
		x = minDist.position[1] + 1
		if x < len(vertices[0]) && !sptSet[[2]int{y,x}] && vertices[y][x].height > minDist.height - 2 {
			vertices[y][x].inf = false
			vertices[y][x].distance = minDist.distance + 1
			verticesWithDistance = append(verticesWithDistance, vertices[y][x])
			if vertices[y][x].height == 0 {
				return vertices[y][x].distance
			}
		}
	}


	return 0
}

func GetInput(filepath string) input {
	var heightMap map[rune]int = make(map[rune]int)
	for h, s := range "abcdefghijklmnopqrstuvwxyz" {
		heightMap[s] = h
	}
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	var m [][]vertex
	stringInput := utilities.ParseFileHandler(file)
	input := input{sptSet: make(map[[2]int]bool)}
	for y, line := range stringInput {
		var l []vertex
		for x, heightStr := range line {
			var h int
			d := 0
			inf := true
			end := false
			pos := [2]int{y, x}
			if heightStr == 'S' {
				h = heightMap['a']
				input.start = pos
			} else if heightStr == 'E' {
				h = heightMap['z']
				end = true
				input.end = pos
			} else {
				h = heightMap[heightStr]
			}
			l = append(l, vertex{height: h, distance: d, inf: inf, position: pos, end: end, visited: false})
			input.sptSet[pos] = false
		}
		m = append(m, l)
	}

	input.data = m

	return input
}

type vertex struct {
	height int
	distance int
	inf bool
	position [2]int
	visited bool
	end bool
}

type input struct {
	data [][]vertex
	start [2]int
	end [2]int
	sptSet map[[2]int]bool
}
