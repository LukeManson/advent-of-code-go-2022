fmt:
	@docker compose run go go fmt ./...

run:
	@docker compose run go go run main.go

test:
	@docker compose run go go test main.go main_test.go

bash:
	@docker compose run go bash