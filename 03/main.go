package main

import (
	"fmt"
	"math"
	"os"
	"utilities"
)

var chars string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func main() {
	input := GetInput("./input.live.txt")
	inputA := GetInputA(input)
	fmt.Println(PartA(inputA))
	inputB := GetInputB(input)
	fmt.Println(PartB(inputB))
}

func PartA(input inputA) int {
	var score int64 = 0
	vm := make(map[int64]int64)

	for _, a := range []rune(chars) {
		packageVal := runeToPackage(rune(a))
		vm[int64(math.Pow(2.0, float64(packageVal)))] = packageVal
	}

	for i := range input.i1 {
		var dupe int64 = 0
		for j := range input.i1[i] {
			for k := range input.i2[i] {
				dupe = input.i1[i][j] & input.i2[i][k]
				if dupe > 0 {
					break
				}
			}
			if dupe > 0 {
				break;
			}
		}
		score += vm[dupe]
	}

	return int(score)
}

func PartB(input inputB) int {
	var score int64 = 0
	vm := make(map[int64]int64)

	for _, a := range []rune(chars) {
		packageVal := runeToPackage(rune(a))
		vm[int64(math.Pow(2.0, float64(packageVal)))] = packageVal
	}

	for group := range input {
		var dupe int64 = 0
		for _, comp0 := range input[group][0] {
			for _, comp1 := range input[group][1] {
				for _, comp2 := range input[group][2] {
					dupe = comp0 & comp1 & comp2
					if dupe > 0 {
						break;
					}
				}
				if dupe > 0 {
					break;
				}
			}
			if dupe > 0 {
				break;
			}
		}
		score += vm[dupe]
	}

	return int(score)
}

func GetInput(filepath string) lines {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	return utilities.ParseFileHandler(file)
}

func GetInputA(lines lines) inputA {
	intInput := inputA{}

	for _, str := range lines {
		var i1 []int64
		var i2 []int64
		l := len(str)
		s1 := str[:l/2]
		s2 := str[l/2:]
		c1 := []rune(s1)
		c2 := []rune(s2)
		for idx := 0; idx < l/2; idx++ {
			p1 := runeToPackage(c1[idx])
			p2 := runeToPackage(c2[idx])
			i1 = append(i1, int64(math.Pow(2.0, float64(p1))))
			i2 = append(i2, int64(math.Pow(2.0, float64(p2))))
		}
		intInput.i1 = append(intInput.i1, i1)
		intInput.i2 = append(intInput.i2, i2)
	}

	return intInput
}

func GetInputB(lines lines) inputB {
	intInput := inputB{}

	for elfNo, str := range lines {
		group := int(math.Floor(float64(elfNo)/3))
		var i []int64
		for idx := range str {
			p := runeToPackage(rune(str[idx]))
			i = append(i, int64(math.Pow(2.0, float64(p))))
		}
		if len(intInput) <= group {
			intInput = append(intInput, [3][]int64{})
		}
		intInput[group][elfNo%3] = i
	}

	return intInput
}

func runeToPackage(c rune) int64 {
	strMap := chars
	for idx, m := range []rune(strMap) {
		if m == c {
			return int64(idx + 1)
		}
	}
	panic("rune not found. rune = " + string(c) + fmt.Sprintf("%d", c))
}

type lines []string

type inputA struct {
	i1 [][]int64
	i2 [][]int64
}

type inputB [][3][]int64