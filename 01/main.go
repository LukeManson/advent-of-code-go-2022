package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"utilities"
)

func main() {
	input := GetInput("./input.live.txt")
	fmt.Println(PartA(input))
	fmt.Println(PartB(input))
}

func PartA(input input) int {
	mostCal := 0
	for _, e := range input.elves {
		total := e.getTotalCalories()
		if total > mostCal {
			mostCal = total
		}
	}
	return mostCal
}

func PartB(input input) int {
	totals := []int{}
	for _, e := range input.elves {
		totals = append(totals, e.getTotalCalories())
	}
	sort.Sort(sort.Reverse(sort.IntSlice(totals)))

	top3 := 0
	for _, val := range totals[:3] {
		top3 += val
	}
	return top3
}

func (e elf) getTotalCalories() int {
	total := 0
	for _, cal := range e.calories {
		total += cal
	}
	return total
}

func GetInput(filepath string) input {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	var i input;
	var e elf;
	stringInput := utilities.ParseFileHandler(file)
	for _, line := range stringInput {
		if line != "" {
			cal, err := strconv.Atoi(line)
			if err != nil {
				panic(err)
			}
			e.calories = append(e.calories, cal)
		} else {
			i.elves = append(i.elves, e)
			e = elf{};
		}
	}
	return i
}

type input struct {
	elves []elf
}

type elf struct {
	calories []int
}