package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"utilities"
)

func main() {
	input := GetInput("./input.live.txt")
	fmt.Println(PartA(input))
	fmt.Println(PartB(input))
}

func PartA(input input) int {
	treeMap := input.data
	count := 0
	for y := 0; y < len(treeMap); y++ {
		// Check from left
		h := -1
		for x := 0; x < len(treeMap[y]); x++ {
			t := &treeMap[y][x]
			// if t.height < tLast {
			// 	break
			// }
			// if t.counted {
			// 	tLast = t.height
			// 	continue
			// }
			if t.height > h {
				if !t.counted {
					count++
					t.counted = true
					t.visible = true
				}
				h = t.height
				fmt.Println(treeMap[y][x])
			}
		}
		
		fmt.Println("y=", y, count)
		
		// Check from right
		h = -1
		for x := len(treeMap[y]) - 1; x >= 0; x-- {
			t := &treeMap[y][x]
			// if t.height < tLast {
			// 	break
			// }
			// if t.counted {
			// 	tLast = t.height
			// 	continue
			// }
			if t.height > h {
				if !t.counted {
					count++
					t.counted = true
					t.visible = true
				}
				fmt.Println(treeMap[y][x])
				h = t.height
			}
		}

		fmt.Println("y=", y, count)
	}

	for x := 0; x < len(treeMap[0]); x++ {
		// Check from top
		h := -1
		for y := 0; y < len(treeMap); y++ {
			t := &treeMap[y][x]
			// if t.height < tLast {
			// 	break
			// }
			// if t.counted {
			// 	tLast = t.height
			// 	continue
			// }
			if t.height > h {
				if !t.counted {
					count++
					t.counted = true
					t.visible = true
				}
				fmt.Println(treeMap[y][x])
				h = t.height
			}
		}

		fmt.Println("x=", x, count)

		// Check from bottom
		h = -1
		for y := len(treeMap) - 1; y >= 0; y-- {
			t := &treeMap[y][x]
			// fmt.Println(t.height)
			// if t.height < tLast {
			// 	break
			// }
			// if t.counted {
			// 	fmt.Println("counted")
			// 	tLast = t.height
			// 	continue
			// }
			if t.height > h {
				if !t.counted {
					count++
					t.counted = true
					t.visible = true
				}
				fmt.Println(treeMap[y][x])
				h = t.height
			}
		}

		fmt.Println("x=", x, count)
	}
	
	return count
}

func PartB(input input) int {
	highScore := 0
	treeMap := input.data
	for y, line := range treeMap {
		for x, t := range line {
			leftScore := 0
			rightScore := 0
			upScore := 0
			downScore := 0
			if x > 0 {
				for x2 := x -1; x2 >= 0; x2-- {
					leftScore++
					if line[x2].height >= t.height {
						break;
					}
				}
			}
			if x < len(line) - 1 {
				for x2 := x + 1; x2 < len(line); x2++ {
					rightScore++
					if line[x2].height >= t.height {
						break;
					}
				}
			}
			if y > 0 {
				for y2 := y -1; y2 >= 0; y2-- {
					downScore++
					if treeMap[y2][x].height >= t.height {
						break;
					}
				}
			}
			if y < len(treeMap) - 1 {
				for y2 := y + 1; y2 < len(treeMap); y2++ {
					upScore++
					if treeMap[y2][x].height >= t.height {
						break;
					}
				}
			}
			t.score = leftScore * rightScore * downScore * upScore

			if t.score > highScore {
				highScore = t.score
			}
		}
	}

	return highScore
}

func GetInput(filepath string) input {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	var forest [][]tree
	stringInput := utilities.ParseFileHandler(file)
	for _, s := range stringInput {
		split := strings.Split(s, "")
		var treeLine []tree
		for _, r := range split {
			i, err := strconv.Atoi(r)
			if err != nil {
				panic(err)
			}
			treeLine = append(treeLine, tree{
				counted: false, 
				visible: false, 
				height: i, 
				score: 0,
			})
		}
		forest = append(forest, treeLine)
	}
	return input{data: forest}
}

type input struct {
	data [][]tree
}

type tree struct {
	counted bool
	visible bool
	score int
	height int
}
