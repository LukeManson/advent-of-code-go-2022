package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"utilities"
)

func main() {
	input := GetInput("./input.live.txt")
	fmt.Println(PartA(input))
	fmt.Println(PartB(input))
}

func PartA(input input) int {
	score := 0
	for _, i := range input.data {
		minA := min(i[0], i[1])
		maxA := max(i[0], i[1])
		minB := min(i[2], i[3])
		maxB := max(i[2], i[3])

		if (minA >= minB && maxA <= maxB) || (minB >= minA && maxB <= maxA) {
			score++
		}
	}
	return score
}

func PartB(input input) int {
	score := 0
	for _, i := range input.data {
		minA := min(i[0], i[1])
		maxA := max(i[0], i[1])
		minB := min(i[2], i[3])
		maxB := max(i[2], i[3])

		if (maxA >= minB && minA <= maxB) || (maxB >= minA && minB <= maxA) {
			score++
		}
	}
	return score
}

func GetInput(filepath string) input {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	stringInput := utilities.ParseFileHandler(file)
	var intArrays [][]int
	for i := range stringInput {
		strings := splitString(stringInput[i])
		var ints []int
		for _, s := range strings {
			j, err := strconv.Atoi(s)
			if err != nil {
				panic(err)
			}
			ints = append(ints, j)
		}
		intArrays = append(intArrays, ints)
	}

	return input{data: intArrays}
}

func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func splitString(input string) []string {
	split1 := strings.Split(input, ",")
	var split2 [][]string
	for _, s := range split1 {
		split2 = append(split2, strings.Split(s, "-"))
	}

	return flatten(split2)
}

func flatten(s [][]string) []string {
	var result []string
	for _, v := range s {
		result = append(result, v...)
	}
	return result
}

type input struct {
	data [][]int
}
