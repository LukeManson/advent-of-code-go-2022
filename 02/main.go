package main

import (
	"fmt"
	"os"
	"strings"
	"utilities"
)

func main() {
	fmt.Println(PartA(GetInput("./input.live.txt", false)))
	fmt.Println(PartB(GetInput("./input.live.txt", true)))
}

func PartA(input input) int {
	score := 0
	for _, t := range input.turns {
		score += t.play()
	}

	return score
}

func PartB(input input) int {
	score := 0
	for _, t := range input.turns {
		score += t.play()
	}

	return score
}

func GetInput(filepath string, partB bool) input {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	stringInput := utilities.ParseFileHandler(file)

	if partB {
		return parseB(stringInput)
	}
	
	return parseA(stringInput)
}

func parseA(stringInput []string) input {
	i := input{}
	for _, line := range stringInput {
		vals := strings.Split(line, " ")
		var p1 int
		var p2 int
		if vals[0] == "A" {
			p1 = rock
		} else if vals[0] == "B" {
			p1 = paper
		} else if vals[0] == "C" {
			p1 = scissors
		}
		
		if vals[1] == "X" {
			p2 = rock
		} else if vals[1] == "Y" {
			p2 = paper
		} else if vals[1] == "Z" {
			p2 = scissors
		}

		i.turns = append(i.turns, turnA{p1:p1, p2:p2})
	}
	return i
}

func parseB(stringInput []string) input {
	i := input{}
	for _, line := range stringInput {
		vals := strings.Split(line, " ")
		var p1 int
		var outcome int
		if vals[0] == "A" {
			p1 = rock
		} else if vals[0] == "B" {
			p1 = paper
		} else if vals[0] == "C" {
			p1 = scissors
		}
		
		if vals[1] == "X" {
			outcome = lose
		} else if vals[1] == "Y" {
			outcome = draw
		} else if vals[1] == "Z" {
			outcome = win
		}

		i.turns = append(i.turns, turnB{p1:p1, out:outcome})
	}
	return i
}

func (t turnA) play() int {
	win := false
	if t.p2 == rock && t.p1 == scissors {
		win = true
	} else if t.p2 == scissors && t.p1 == rock {
		win = false
	} else {
		win = t.p2 > t.p1
	}

	score := t.p2
	if win {
		score += 6
	} else if t.p2 == t.p1 {
		score += 3
	}

	return score
}

func (t turnB) play() int {
	var p2 int
	if t.out == draw {
		p2 = t.p1
	} else if t.out == win && t.p1 == scissors {
		p2 = rock
	} else if t.out == lose && t.p1 == rock {
		p2 = scissors
	} else if t.out == win {
		p2 = t.p1 + 1
	} else {
		p2 = t.p1 -1
	}

	score := p2
	score += t.out

	return score
}

const (
	rock = iota + 1
	paper
	scissors
)

const (
	lose = 0
	draw = 3
	win = 6
)

type input struct {
	turns []turn
}

type turnA struct {
	p1 int
	p2 int
}

type turnB struct {
	p1 int
	out int
}

type turn interface {
	play() int
}